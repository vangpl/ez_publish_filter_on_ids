<?php

# Receives an array of object-ids to filter out from fetch
# Use in fetch (example):
# $example=fetch( 'content', 'tree',
#									hash(	'parent_node_id', 						<parent_node_id>,
#												'sort_by',										array( 'published', false() ),
#												'extended_attribute_filter',	hash(
#																												'id', 'filter_on_ids',
#																												'params', hash(
#																																		'ids', array(<(int) id to exclude>[,<(int) another id to exclude>])
#																																	)
#																											),
#												'limit',											10
#											)
#								)

class filterOnIds
{
	public function filterOnIds( $params ) {
		$db = eZDB::instance();

		$joins = ' ezcontentobject_tree.contentobject_id = ezcontentobject_attribute.contentobject_id AND ';

		if( isset( $params['ids'] ) ) {

			foreach( $params['ids'] as $id ) {

				$joins .= 'ezcontentobject_attribute.contentobject_id != ' . $db->escapeString( $id ) . ' AND ';

			}

		}

		return array(
			'tables' 	=> ', ezcontentobject_attribute ',
			'joins'		=> $joins,
			'columns'	=> null
		);
	}
}

?>